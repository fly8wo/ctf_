#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

uint64_t murmur_hash_64_inverse(uint64_t h, uint64_t seed)
{
	const uint64_t m = 0xc6a4a7935bd1e995ULL;
	const uint64_t minv = 0x5f7a0ea7e59b19bdULL; // Multiplicative inverse of m under % 2^64
	const int r = 47;

	h ^= h >> r;
	h *= minv;
	h ^= h >> r;
	h *= minv;

	uint64_t hforward = seed ^ (8 * m);
	uint64_t k = h ^ hforward;

	k *= minv;
	k ^= k >> r;
	k *= minv;

	return k;
}


int main()
{
    printf("%lu",murmur_hash_64_inverse(0x3dba201d32b78891, 0xc70f6907));
}
