from pwn import *
context.arch = 'aarch64'

shellcode = asm('''
 add x1, x1, 0x30
 br x1
''')

print(shellcode)
